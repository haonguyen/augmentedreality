//
//  OverlayVideo.m
//  ARiseHybrid
//
//  Created by haonguyen on 6/3/14.
//  Copyright (c) 2014 Knorex. All rights reserved.
//

#import "OverlayVideo.h"

#import "ARiseImageUtils.h"
#import "ARiseMathUtils.h"
#import "ARiseMBProgressHUD.h"
#import "ARiseUtils.h"
#import "MIMAS.h"

@implementation OverlayVideo
{
    ARiseMBProgressHUD *loadingProgressDialog;
}

@synthesize state;

- (id)initJSON:(NSDictionary *)json
{
    [self showProgressDialog];
    
    if (self) {
        
        NSString *urlValue = [json objectForKey:@"url"];

        if ([urlValue hasPrefix:@"file:///"]) {
            
            NSString *filename = [[[urlValue substringFromIndex:8] lastPathComponent] stringByDeletingPathExtension];
            NSString *extension = [[[urlValue substringFromIndex:8] lastPathComponent] pathExtension];
            
            NSString *resourcePath = [[NSBundle mainBundle] pathForResource:filename ofType:extension];
            
            if (resourcePath) {
                videoURL = [[NSURL alloc] initFileURLWithPath:[[NSBundle mainBundle] pathForResource:filename ofType:extension]];
            } else {
                [ARiseUtils log:@"Resource not found."];
            }
        } else {
            videoURL = [NSURL URLWithString:urlValue];
        }
        
        if (videoURL != NULL) {
            
            // parse data
            width = [[json objectForKey:@"width"] intValue] * 320 / [ARiseUtils getDeviceSize].width;
            height = [[json objectForKey:@"height"] intValue] * 480 / [ARiseUtils getDeviceSize].height;
            offsetx = [[json objectForKey:@"offsetx"] intValue];
            offsety = [[json objectForKey:@"offsety"] intValue];
            autoplay = [[json objectForKey:@"autoplay"] boolValue];
            callback = [[json objectForKey:@"callback"] boolValue];
            delay = [[json objectForKey:@"delay"] intValue];
            
            // default
            if (!autoplay) {
                autoplay = TRUE;
            }
            if (!callback) {
                callback = TRUE;
            }
            if (!delay) {
                delay = -1;
            }
            
            // prepare video view
            [[self view] setFrame:CGRectMake(([ARiseUtils getDeviceSize].width - width) / 2,
                                             ([ARiseUtils getDeviceSize].height - height) / 2,
                                             width,
                                             height)];
            
            [self setControlStyle:MPMovieControlStyleNone];
            [self setScalingMode:MPMovieScalingModeFill];
            [self setRepeatMode:NO];
            [self setShouldAutoplay:autoplay];
            [[self view] setMultipleTouchEnabled:NO];
            [[self view] setHidden:YES];
            [self setContentURL:videoURL];
            [self prepareToPlay];
            
            [self initConstantValues];
            
            entranceScale = 0.f;
            state = END;
            lerpStep = 1;
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoEnd:) name:MPMoviePlayerPlaybackDidFinishNotification object:self];
            
            // prepare play button
            opaqueLayer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
            [opaqueLayer setBackgroundColor:[[UIColor grayColor] colorWithAlphaComponent:0.6f]];
            
            playButton = [[UIImageView alloc] initWithImage:[ARiseImageUtils imageFromARiseBundleWithName:@"play_button.png"]];
            [playButton setFrame:CGRectMake(width / 2 - MIN(width, height) / 4,
                                            height / 2 - MIN(width, height) / 4,
                                            MIN(width, height) / 2,
                                            MIN(width, height) / 2)];
            [[playButton layer] setZPosition:1100.f];
            [[self view] addSubview:opaqueLayer];
            [[self view] addSubview:playButton];
            [[self backgroundView] setAlpha:0.4f];
            
            // display the video eventually
            [NSTimer scheduledTimerWithTimeInterval:2
                                             target:self
                                           selector:@selector(startAppearAndPlay)
                                           userInfo:nil
                                            repeats:NO];
        } else {
            [self hideProgressDialog];
            self = NULL;
        }   
    }
    return self;
}

- (void)initConstantValues
{
    flipY = GLKMatrix4Make(1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
    scale = GLKMatrix4Make([ARiseUtils getDeviceSize].width / 2, 0, 0, 0, 0, [ARiseUtils getDeviceSize].height / 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
    
    intMatrix = [self convertIntrinsicToCameraProjectionMatrix];
    
    lostTrackedQuaternion = CC3Vector4Make(0, 0, 0, 1);
    lostTrackedPosition = CC3VectorMake(0.f, 0.f, -[ARiseMathUtils getFocalLength]);
}

- (void)videoEnd:(NSNotificationCenter*)pNotification
{
    // player ends
    
    [self requestPause];
    [self stop];
}

- (GLKMatrix4)getOrthoMatrixFromScreenSize
{
    float left = -[ARiseUtils getDeviceSize].width / 2;
    float bottom = -[ARiseUtils getDeviceSize].height / 2;
    float right = [ARiseUtils getDeviceSize].width / 2;
    float top = [ARiseUtils getDeviceSize].height / 2;
    float far = 1e6f;
    float near = 1.f;
    
    // Ortho projection
    return GLKMatrix4MakeOrtho(left, right, bottom, top, near, far);
}

- (GLKMatrix4)getIntrinsicMatrix
{
    float scaleX = [ARiseUtils getDeviceSize].width / 240.f;
    float scaleY = [ARiseUtils getDeviceSize].height / 320.f;
    float alpha = [ARiseMathUtils getFocalLength] * scaleX;
    float beta = [ARiseMathUtils getFocalLength] * scaleY;
    float near = 1.f;
    float far = 1e6f;
    
    return GLKMatrix4Make(alpha , 0      , 0        ,   0,
                          0     , beta   , 0        ,   0,
                          0     , 0      , near+far ,   -1,
                          0     , 0      , near*far ,   0);
    
}

- (GLKMatrix4)convertIntrinsicToCameraProjectionMatrix
{
    return GLKMatrix4Multiply([self getOrthoMatrixFromScreenSize], [self getIntrinsicMatrix]);
}

- (void)tryStartPlaying
{
    @synchronized(self) {
        if (![[self view] isHidden]) {
            if (state == END) {
                if ([self isPreparedToPlay]) {
                    [self play];
                    state = PLAYING;
                    [self hidePlayButton];
                }
            }
        }
    }
}

- (void)requestPause
{
    @synchronized(self) {
        if (state == PLAYING) {
            [self pause];
            state = PAUSE;
            [self showPlayButton];
        }
    }
}

- (void)requestResume
{
    @synchronized(self) {
        if (state == PAUSE) {
            [self play];
            state = PLAYING;
            [self hidePlayButton];
        }
    }
}

- (void)toggleState
{
    @synchronized (self) {
        switch (state) {
            case PAUSE:
                [self requestResume];
                break;
            case PLAYING:
                [self requestPause];
                break;
            default:
                break;
        }
    }
}

- (void)performLostTrackedTransformation
{
    if (entranceScale < 1.f && ![[self view] isHidden]) {
        entranceScale += 1.f/ TRANSITION_STEPS;
    }
    
    if (onTracked) {
        lerpStep = TRANSITION_STEPS + 1;
        onTracked = NO;
    } else {
        if (lerpStep > 1) {
            lerpStep--;
        }
    }
    
    GLKMatrix4 scaleMatrix = GLKMatrix4Scale(scale, entranceScale, entranceScale, 1);
    
    curQuaternion = [ARiseMathUtils slerpFromQuaternion:curQuaternion toQuaternion:lostTrackedQuaternion withStep:lerpStep];
    curPosition = [ARiseMathUtils lerpFromVector:curPosition toVector:lostTrackedPosition withStep:lerpStep];
    
    CC3GLMatrix *a = [CC3GLMatrix identity];
    [a populateFromQuaternion:curQuaternion];
    CC3Vector rightVector = [a extractRightDirection];
    CC3Vector upVector = [a extractUpDirection];
    CC3Vector forwardVector = [a extractForwardDirection];
    
    GLKMatrix4 curExtrinsicMatrix = GLKMatrix4Make(rightVector.x, upVector.x,     forwardVector.x,    0,
                                                   rightVector.y, upVector.y,     forwardVector.y,    0,
                                                   rightVector.z, upVector.z,     forwardVector.z,    0,
                                                   curPosition.x, curPosition.y,  curPosition.z,      1);
    curExtrinsicMatrix = GLKMatrix4Translate(curExtrinsicMatrix,
                                             offsety * (lerpStep - 1) / TRANSITION_STEPS,
                                             offsetx * (lerpStep - 1) / TRANSITION_STEPS,
                                             0);

    GLKMatrix4 camCalibrationMatrix = GLKMatrix4Multiply(flipY,
                                                         GLKMatrix4Multiply(scaleMatrix,
                                                                            GLKMatrix4Multiply(intMatrix,
                                                                                               GLKMatrix4Multiply(curExtrinsicMatrix, flipY))));
    
    [self performCATransform3DUsingGLKMatrix4:camCalibrationMatrix];
}

- (void)update:(double *)poseMatrix
{
    if (entranceScale < 1.f && ![[self view] isHidden]) {
        entranceScale += 1.f/ TRANSITION_STEPS;
    }
    
    if (!onTracked) {
        lerpStep = TRANSITION_STEPS + 1;
        onTracked = YES;
    } else {
        if (lerpStep > 1) {
            lerpStep--;
        }
    }
    
    [self startAppearAndPlay];
    
    GLKMatrix4 scaleMatrix = GLKMatrix4Scale(scale, entranceScale, entranceScale, 1);
    
    float pose[3][4];
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            pose[j][i] = poseMatrix[i*3+j];
        }
    }
    for (int i = 0; i < 3; i++) {
        pose[i][3] = poseMatrix[9+i];
    }
    
    CC3GLMatrix *newMatrix = [CC3GLMatrix matrixFromGLMatrix:(float*)pose];
    
    CC3Vector4 quat = [newMatrix extractQuaternion];
    
    CC3Vector4 camQuaternion;
    camQuaternion.w =  quat.w;
    camQuaternion.x = -quat.x;
    camQuaternion.y = quat.y;
    camQuaternion.z = -quat.z;
    
    camQuaternion = [ARiseMathUtils normalizeQuaternion:camQuaternion];
    
    curQuaternion = [ARiseMathUtils slerpFromQuaternion:curQuaternion toQuaternion:camQuaternion withStep:lerpStep];
    curPosition = [ARiseMathUtils lerpFromVector:curPosition toVector:cc3v(poseMatrix[9], -poseMatrix[10], -poseMatrix[11]) withStep:lerpStep];
    
    CC3GLMatrix *a = [CC3GLMatrix identity];
    [a populateFromQuaternion:curQuaternion];
    CC3Vector rightVector = [a extractRightDirection];
    CC3Vector upVector = [a extractUpDirection];
    CC3Vector forwardVector = [a extractForwardDirection];
    
    GLKMatrix4 curExtrinsicMatrix = GLKMatrix4Make(rightVector.x,  upVector.x,     forwardVector.x,    0,
                                                   rightVector.y,  upVector.y,     forwardVector.y,    0,
                                                   rightVector.z,  upVector.z,     forwardVector.z,    0,
                                                   curPosition.x,  curPosition.y,  curPosition.z,  1);
    curExtrinsicMatrix = GLKMatrix4Translate(curExtrinsicMatrix,
                                             offsety * (TRANSITION_STEPS - lerpStep + 1) / TRANSITION_STEPS,
                                             offsetx * (TRANSITION_STEPS - lerpStep + 1) / TRANSITION_STEPS,
                                             0);
    
    // camCalibrationMatrix = flipY x scale x intMatrix x extMatrix x flipY
    GLKMatrix4 camCalibrationMatrix = GLKMatrix4Multiply(flipY,
                                                         GLKMatrix4Multiply(scaleMatrix,
                                                                            GLKMatrix4Multiply(intMatrix,
                                                                                               GLKMatrix4Multiply(curExtrinsicMatrix, flipY))));
    
    [self performCATransform3DUsingGLKMatrix4:camCalibrationMatrix];
}

- (void)hidePlayButton
{
    [opaqueLayer setHidden:YES];
    [playButton setHidden:YES];
}

- (void)showPlayButton
{
    [opaqueLayer setHidden:NO];
    [playButton setHidden:NO];
}

- (void)performCATransform3DUsingGLKMatrix4:(GLKMatrix4)glkMatrix
{
    CATransform3D caTransform;
    caTransform.m11 = glkMatrix.m[0];
    caTransform.m12 = glkMatrix.m[1];
    caTransform.m13 = glkMatrix.m[2];
    caTransform.m14 = glkMatrix.m[3];
    caTransform.m21 = glkMatrix.m[4];
    caTransform.m22 = glkMatrix.m[5];
    caTransform.m23 = glkMatrix.m[6];
    caTransform.m24 = glkMatrix.m[7];
    caTransform.m31 = glkMatrix.m[8];
    caTransform.m32 = glkMatrix.m[9];
    caTransform.m33 = glkMatrix.m[10];
    caTransform.m34 = glkMatrix.m[11];
    caTransform.m41 = glkMatrix.m[12];
    caTransform.m42 = glkMatrix.m[13];
    caTransform.m43 = glkMatrix.m[14];
    caTransform.m44 = glkMatrix.m[15];
    dispatch_async(dispatch_get_main_queue(), ^{
        [CATransaction begin];
        [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
        [[[self view] layer] setTransform:caTransform];
        [CATransaction commit];
    });
}

- (void)showProgressDialog
{
    loadingProgressDialog = [ARiseMBProgressHUD showHUDAddedTo:[[self view] superview] animated:YES];
    [loadingProgressDialog setMode:MBProgressHUDModeText];
    [loadingProgressDialog setLabelText:@"Loading video..."];
    [loadingProgressDialog setRemoveFromSuperViewOnHide:YES];
    [loadingProgressDialog setUserInteractionEnabled:YES];
}

- (void)hideProgressDialog
{
    if (loadingProgressDialog) {
        if (![loadingProgressDialog isHidden])
            [loadingProgressDialog hide:YES];
    }
}

- (void)startAppearAndPlay
{
    if ([[self view] isHidden]) {
        [self hideProgressDialog];
        [[self view] setHidden:NO];
    }
    [self tryStartPlaying];
}

@end