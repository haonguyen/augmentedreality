//
//  FullscreenVideoViewController.m
//  ARiseHybrid
//
//  Created by haonguyen on 7/3/14.
//  Copyright (c) 2014 Knorex. All rights reserved.
//

#import "OverlayVideoFullscreen.h"

@interface OverlayVideoFullscreen() {
    
}
@end

@implementation OverlayVideoFullscreen

- (id)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(doneButtonClick:)
                                                     name:MPMoviePlayerWillExitFullscreenNotification
                                                   object:nil];
        
        [[self moviePlayer] setScalingMode:MPMovieScalingModeAspectFit];
        [[self moviePlayer] setControlStyle:MPMovieControlStyleFullscreen];
        [[self moviePlayer] setFullscreen:YES];
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)doneButtonClick:(NSNotification *)pNotification
{
    [[self navigationController] dismissViewControllerAnimated:YES
                                                    completion:nil];
}

- (void)setVideoUrl:(NSURL *)url
{
    videoUrl = url;
}

- (NSURL *)getVideoUrl
{
    return videoUrl;
}

@end