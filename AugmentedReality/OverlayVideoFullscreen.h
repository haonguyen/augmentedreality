//
//  FullscreenVideoViewController.h
//  ARiseHybrid
//
//  Created by haonguyen on 7/3/14.
//  Copyright (c) 2014 Knorex. All rights reserved.
//

#import "ARiseUtils.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <UIKit/UIKit.h>

@interface OverlayVideoFullscreen : MPMoviePlayerViewController
{
    NSURL *videoUrl;
}

- (void)setVideoUrl:(NSURL *)url;
- (NSURL *)getVideoUrl;

@end