//
//  Overlay3DModel.h
//  ARiseHybrid
//
//  Created by haonguyen on 3/3/14.
//  Copyright (c) 2014 Knorex. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CC3Node.h"
#import "CC3PODResourceNode.h"
#import "CC3ActionInterval.h"
#import "CC3GLMatrix.h"


@interface Overlay3DModel : CC3Node {
    
    CGFloat focalLength;
    
    CGFloat hAngle;
    CGFloat vAngle;
    CGFloat scaleFactor;

    CGFloat entranceScale;
    
    CC3PODResourceNode *model;
    CC3Box boundingBox;
    CGFloat autoScaleFactor;
    
    CC3Quaternion curQuaternion;
    CC3Vector curPosition;
    
    CC3Quaternion lostTrackedQuaternion;
    CC3Vector lostTrackedPosition;
    
    BOOL onTracked;
    int transitionStep;
    
    NSString *asset;
    float scale;
    int offsetx;
    int offsety;
    int mode;
}

- (void)performHorizontalRotation:(CGFloat)angle;
- (void)performVerticalRotation:(CGFloat)angle;

- (void)setScaleFactor:(CGFloat)scale;
- (CGFloat)getScaleFactor;

- (void)reset;
- (void)performTransformationFromPoseMatrix:(double *)poseMatrix;
- (void)performLostTrackedTransformation;

- (BOOL)setPODResourceFromJSON:(NSDictionary *)json;

@end
