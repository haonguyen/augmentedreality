//
//  OverlayVideo.h
//  ARiseHybrid
//
//  Created by haonguyen on 6/3/14.
//  Copyright (c) 2014 Knorex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>
#import <MediaPlayer/MediaPlayer.h>

#import "CC3GLMatrix.h"


enum OverlayVideoState {
    END,
    PLAYING,
    PAUSE
};

@interface OverlayVideo : MPMoviePlayerController {
    
    GLKMatrix4 flipY;
    GLKMatrix4 scale;
    GLKMatrix4 intMatrix;
    GLKMatrix4 lostTrackedMatrix;
    CC3Quaternion lostTrackedQuaternion;
    CC3Vector lostTrackedPosition;
    
    NSURL *videoURL;
    int width;
    int height;
    int offsetx;
    int offsety;
    BOOL autoplay;
    BOOL callback;
    int delay;
    
    CGFloat entranceScale;
    
    CC3Quaternion curQuaternion;
    CC3Vector curPosition;
    
    int lerpStep;
    BOOL onTracked;
    
    UIImageView *playButton;
    UIView *opaqueLayer;
}

@property (atomic) enum OverlayVideoState state;

- (id)initJSON:(NSDictionary*)json;

- (void)update:(double*)poseMatrix;
- (void)performLostTrackedTransformation;

- (void)requestPause;
- (void)requestResume;
- (void)toggleState;

@end
