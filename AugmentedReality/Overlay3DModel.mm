//
//  Overlay3DModel.m
//  ARiseHybrid
//
//  Created by haonguyen on 3/3/14.
//  Copyright (c) 2014 Knorex. All rights reserved.
//

#import "Overlay3DModel.h"

#import "ARiseUtils.h"
#import "ARiseMathUtils.h"
#import "ARiseMBProgressHUD.h"
#import "NSData+CocoaDevUsersAdditions.h"
#import "NSData+MD5Digest.h"
#import "NSFileManager+Tar.h"

#import "ccMacros.h"
#import "CC3MeshNode.h"



@implementation Overlay3DModel {
    CC3Quaternion gestureQuaternion;
    ARiseMBProgressHUD *loadingProgressPanel;
}

- (BOOL)setPODResourceFromJSON:(NSDictionary *)json
{
    [self showProgressDialog];
    
    // parse data
    asset = [json objectForKey:@"asset"];
    scale = [[json objectForKey:@"scale"] floatValue];
    offsetx = [[json objectForKey:@"offsetx"] intValue];
    offsety = [[json objectForKey:@"offsety"] intValue];
    mode = [[json objectForKey:@"mode"] intValue];
    
    if (asset != nil) {
        BOOL fileExists;
        NSString *file = nil;
        if ([asset hasPrefix:@"file:///"]) {
            NSString *filename = [[asset substringFromIndex:8] stringByDeletingPathExtension];
            NSString *resourcePath = [[NSBundle mainBundle] pathForResource:filename
                                                                     ofType:@"pod"];
            if (resourcePath) {
                fileExists = YES;
                file = [asset substringFromIndex:8];
            } else {
                fileExists = NO;
            }
        } else {
            
            // check file existence
            NSString *isnapAssetPath = [ARiseUtils getARiseAssetPath];
            
            NSString *filename = [asset lastPathComponent];
            filename = [filename substringToIndex:([filename length] - 7)];
            
            NSString *modelDirectory = [isnapAssetPath stringByAppendingPathComponent:filename];
            NSString *modelPath = [modelDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", filename, @"pod"]];
            
            fileExists = [[NSFileManager defaultManager] fileExistsAtPath:modelPath];
            file = modelPath;
            
            if (!fileExists) {
                [ARiseUtils log:[NSString stringWithFormat:@"File does not exist at: %@. Start downloading...", file]];
                [self download3DModelPackage];
            }
            
            fileExists = [[NSFileManager defaultManager] fileExistsAtPath:modelPath];
        }
        
        if (fileExists) {
            if (loadingProgressPanel) {
                [loadingProgressPanel setLabelText:@"Creating 3D model..."];
            }
            
            [self reset];
            
            [self removeChild:model];
            
            model = [CC3PODResourceNode nodeWithName:@"Overlay3DModel"];
            [model setResource:[CC3PODResource resourceFromFile:file]];
            
            
            if ([model resource] == nil) {
                [self hideProgressDialog];
                return NO;
            } else {
                boundingBox = [model boundingBox];
                lostTrackedPosition = CC3VectorMake(0.f, 0.f, -[ARiseMathUtils getFocalLength] - boundingBox.minimum.z);
                
                CCActionInterval *action = [CC3Animate actionWithDuration:2.0f];
                [model runAction:[CCRepeatForever actionWithAction:action]];
                
                [self calculateAutoscaleFactor];
                
//                [model setIsTouchEnabled:YES]; // deprecated
                [model setTouchEnabled:YES];
                
                [self addChild:model];
                
                [self hideProgressDialog];
                
                // display this 3D model eventually
                [NSTimer scheduledTimerWithTimeInterval:2
                                                 target:self
                                               selector:@selector(show:)
                                               userInfo:nil
                                                repeats:NO];
                
                return YES;
            }
        } else {
            [self hideProgressDialog];
            return NO;
        }
    }
    return NO;
}

- (void)performHorizontalRotation:(CGFloat)angle
{
    hAngle += -angle;
}

- (void)performVerticalRotation:(CGFloat)angle
{
    vAngle += -angle;
}

- (void)setScaleFactor:(CGFloat)s
{
    scaleFactor *= s;
}

- (CGFloat)getScaleFactor
{
    return scaleFactor;
}

- (void)reset
{
    hAngle = 0;
    vAngle = 0;
    scaleFactor = 1;
    
    autoScaleFactor = 1;
    
    focalLength = [ARiseMathUtils getFocalLength];
    
    lostTrackedQuaternion.w = 1;
    lostTrackedQuaternion.x = 0;
    lostTrackedQuaternion.y = 0;
    lostTrackedQuaternion.z = 0;
    
    lostTrackedPosition = CC3VectorMake(0.f, 0.f, -[ARiseMathUtils getFocalLength]);
    
    onTracked = YES;
    entranceScale = 0.f;
    transitionStep = 1;
    
    [self setVisible:NO];
}

- (void)performTransformationFromPoseMatrix:(double*)poseMatrix
{
    if (poseMatrix != NULL && model != nil && [model resource] != nil) {
        
        [self setVisible:YES];
        
        if (entranceScale < 1.f && [self visible])
            entranceScale += 1.f / TRANSITION_STEPS;
        
        // update transition step
        if (!onTracked) {
            transitionStep = TRANSITION_STEPS + 1;
            onTracked = YES;
        } else {
            if (transitionStep > 1) {
                transitionStep--;
            }
        }
        
        // perform lerp transition
        float pose[3][4];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                pose[j][i] = poseMatrix[i*3+j];
            }
        }
        for (int i = 0; i < 3; i++) {
            pose[i][3] = poseMatrix[9+i];
        }
        
        CC3GLMatrix *newMatrix = [CC3GLMatrix matrixFromGLMatrix:(float *)pose];
        
        CC3Vector4 quart = [newMatrix extractQuaternion];
        
        CC3Vector4    camQuaternion;
        camQuaternion.w =  quart.w;
        camQuaternion.x = -quart.x;
        camQuaternion.y = quart.y;
        camQuaternion.z = quart.z;
        
        curQuaternion = [ARiseMathUtils slerpFromQuaternion:curQuaternion toQuaternion:camQuaternion withStep:transitionStep];
        
        float tx = [self rightDirection].x * offsetx + [self upDirection].x * offsety;
        float ty = [self rightDirection].y * offsetx + [self upDirection].y * offsety;
        float tz = [self rightDirection].z * offsetx + [self upDirection].z * offsety;
        
        curPosition = [ARiseMathUtils lerpFromVector:curPosition
                                            toVector:cc3v(poseMatrix[9] + tx, -poseMatrix[10] + ty, -poseMatrix[11] - boundingBox.minimum.z + tz)
                                            withStep:transitionStep];

        // rotate by parent axis
        curQuaternion   = [ARiseMathUtils multiplyQuaternion:curQuaternion onAxis:CC3Vector4Make(1, 0, 0, 0) byDegress:vAngle];
        curQuaternion   = [ARiseMathUtils multiplyQuaternion:curQuaternion onAxis:CC3Vector4Make(0, 1, 0, 0) byDegress:hAngle];
        
        [self setScale:cc3v(autoScaleFactor*scaleFactor*entranceScale, autoScaleFactor*scaleFactor*entranceScale, autoScaleFactor*scaleFactor*entranceScale)];
        
        [self setQuaternion:curQuaternion];
        [self setLocation:curPosition];
    }
}

- (void)performLostTrackedTransformation
{
    if (model == nil || [model resource] == nil)
        return;
    
    if (entranceScale < 1.f && [self visible])
        entranceScale += 1.f / TRANSITION_STEPS;
    
    // update transition step
    if (onTracked) {
        transitionStep = TRANSITION_STEPS + 1;
        onTracked = NO;
    } else {
        if (transitionStep > 1) {
            transitionStep--;
        }
    }
    
    curQuaternion = [ARiseMathUtils slerpFromQuaternion:curQuaternion toQuaternion:lostTrackedQuaternion withStep:transitionStep];
    curPosition = [ARiseMathUtils lerpFromVector:curPosition toVector:lostTrackedPosition withStep:transitionStep];
    
    curQuaternion   = [ARiseMathUtils multiplyQuaternion:curQuaternion onAxis:CC3Vector4Make(1, 0, 0, 0) byDegress:vAngle];
    curQuaternion   = [ARiseMathUtils multiplyQuaternion:curQuaternion onAxis:CC3Vector4Make(0, 1, 0, 0) byDegress:hAngle];
    
    [self setScale:cc3v(autoScaleFactor*scaleFactor*entranceScale, autoScaleFactor*scaleFactor*entranceScale, autoScaleFactor*scaleFactor*entranceScale)];
    [self setQuaternion:curQuaternion];
    [self setLocation:curPosition];
}

- (void)calculateAutoscaleFactor
{
    @try {
        autoScaleFactor = MIN(240.f / (boundingBox.maximum.x - boundingBox.minimum.x), 320.f / (boundingBox.maximum.y - boundingBox.minimum.y)) / 2.f;
    } @catch (NSException *e) {
        [ARiseUtils log:[NSString stringWithFormat:@"Error while calculating autoscale factor. Reason: %@", e.reason]];
        autoScaleFactor = 1;
    } @finally {
        [self setScale:cc3v(autoScaleFactor, autoScaleFactor, autoScaleFactor)];
    }
}

- (CC3Vector4)performHamiltonProductWithQuaternion:(CC3Vector4)q andVector:(CC3Vector4)v
{
    float lengthQ = sqrtf((q.x * q.x) + (q.y * q.y) + (q.z * q.z) + (q.w * q.w));
    CC3Vector4 nQ = CC3Vector4Make(q.x / lengthQ, q.y / lengthQ, q.z / lengthQ, q.w / lengthQ);
    
    CC3Vector4 tmp, tmp1;
    tmp.w = nQ.w * v.w - nQ.x * v.x - nQ.y * v.y - nQ.z * v.z;
    tmp.x = nQ.w * v.x + nQ.x * v.w + nQ.y * v.z - nQ.z * v.y;
    tmp.y = nQ.w * v.y - nQ.x * v.z + nQ.y * v.w + nQ.z * v.x;
    tmp.z = nQ.w * v.z + nQ.x * v.y - nQ.y * v.x + nQ.z * v.w;
    
    tmp1.w = tmp.w * nQ.w - tmp.x * -nQ.x - tmp.y * -nQ.y - tmp.z * -nQ.z;
    tmp1.x = tmp.w * -nQ.x + tmp.x * nQ.w + tmp.y * -nQ.z - tmp.z * -nQ.y;
    tmp1.y = tmp.w * -nQ.y - tmp.x * -nQ.z + tmp.y * nQ.w + tmp.z * -nQ.x;
    tmp1.z = tmp.w * -nQ.z + tmp.x * -nQ.y - tmp.y * -nQ.x + tmp.z * nQ.w;
    
    return tmp1;
}

- (void)show:(NSTimer *)timer
{
    [self setVisible:YES];
}

- (void)download3DModelPackage
{
    if (loadingProgressPanel) {
        [loadingProgressPanel setLabelText:@"Downloading 3D model..."];
    }
    
    NSURL *url = [NSURL URLWithString:asset];
    NSURLRequest *request = [NSURLRequest requestWithURL:url
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:5.0];
    NSError *downloadErr = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&downloadErr];
    
    NSString *ariseAssetPath = [ARiseUtils getARiseAssetPath];
    if (downloadErr == nil && [data length] > 0) {
        NSData *extractedData = [data gzipInflate];
        NSError *extractErr = nil;
        [[NSFileManager defaultManager] createFilesAndDirectoriesAtPath:ariseAssetPath
                                                            withTarData:extractedData
                                                                  error:&extractErr];
        if (extractErr != nil) {
            [ARiseUtils log:[NSString stringWithFormat:@"Error while extracting overlay 3D model package: %@", [extractErr localizedDescription]]];
        }
    } else {
        [ARiseUtils log:[NSString stringWithFormat:@"Error while downloading overlay 3D model package: %@", [downloadErr localizedDescription]]];
    }
}

- (CC3Quaternion)multiplyQuaternion:(CC3Quaternion)q1 withQuaternion:(CC3Quaternion)q2
{
    CC3Quaternion result;
    result.w = q1.w * q2.w - q1.x * q2.x - q1.y * q2.y - q1.z * q2.z;
    result.x = q1.w * q2.x + q1.x * q2.w + q1.y * q2.z - q1.z * q2.y;
    result.y = q1.w * q2.y - q1.x * q2.z + q1.y * q2.w + q1.z * q2.x;
    result.z = q1.w * q2.z + q1.x * q2.y - q1.y * q2.x + q1.z * q2.w;
    return result;
}

#pragma mark -
#pragma mark ProgressDialog

- (void)showProgressDialog
{
    loadingProgressPanel = [ARiseMBProgressHUD showHUDAddedTo:[[[self scene] controller] view] animated:YES];
    [loadingProgressPanel setMode:MBProgressHUDModeText];
    [loadingProgressPanel setLabelText:@"Loading 3D model..."];
    [loadingProgressPanel setRemoveFromSuperViewOnHide:YES];
    [loadingProgressPanel setUserInteractionEnabled:YES];
}

- (void)hideProgressDialog
{
    if (loadingProgressPanel) {
        if (![loadingProgressPanel isHidden])
            [loadingProgressPanel hide:YES];
    }
}

@end
